package exaprgeva23;

import java.util.ArrayList; //6

public class Exa23ArrayList {

  public static void main(String[] args) {

    ArrayList<String> nombres = new ArrayList<String>(); // 1

    nombres.add("Ana");
    nombres.add("Luisa");
    nombres.add("Felipe");
    System.out.println(nombres); // [Ana, Luisa, Felipe]
    nombres.add(1, "Pablo"); // 2
    System.out.println(nombres); // [Ana, Pablo, Luisa, Felipe]
    nombres.remove(0); // 3
    System.out.println(nombres); // [Pablo, Luisa, Felipe]
    nombres.set(0, "Alfonso"); // 4
    System.out.println(nombres); // [Alfonso, Luisa, Felipe]
    String s = nombres.get(1); // 5
    String ultimo = nombres.get(nombres.size() - 1);
    System.out.println(s + " " + ultimo);  // Luisa Felipe 
  }
}
/* EJECICION:
 [Ana, Luisa, Felipe]
 [Ana, Pablo, Luisa, Felipe]
 [Pablo, Luisa, Felipe]
 [Alfonso, Luisa, Felipe]
 Luisa Felipe
 */
